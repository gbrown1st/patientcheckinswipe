package com.FirstAvailable.PatientCheckIn;

import com.google.gson.JsonElement;
import com.magtek.mobile.android.mtlib.IMTCardData;
import com.magtek.mobile.android.mtlib.MTConnectionType;
import com.magtek.mobile.android.mtlib.MTSCRA;
import com.magtek.mobile.android.mtlib.MTSCRAEvent;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MedicareSwipeActivity extends Activity implements OnClickListener {

	
	public ProgressDialog pd;
	public static final String PREFS_NAME = "MyPrefsFile";
	public static SharedPreferences settings = null;
	public static String surgery_id;
	public static String pms;
	public static String url;
	public PubNub pubnub;
	public String android_id;
	
	private final static String TAG = MainActivity.class.getSimpleName();

	private MTSCRA m_scra;
	private Handler m_scraHandler = new Handler(new SCRAHandlerCallback()); 
	
    
	private class SCRAHandlerCallback implements Callback  {

		@Override
		public boolean handleMessage(Message msg) {
			try
        	{        		
    	        Log.i(TAG, "*** Callback " + msg.what);        
			switch (msg.what)
    		{
    			case MTSCRAEvent.OnDataReceived:
				OnCardDataReceived((IMTCardData) msg.obj);
				break;
    		}
		}
    	catch (Exception ex)
    	{
    		
    	}
    	
    	return true;
		}
		
	}
	
   
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.swipe);
        
        m_scra = new MTSCRA(this, m_scraHandler);
        m_scra.setConnectionType(MTConnectionType.Audio);
        m_scra.openDevice();
        
        settings = getSharedPreferences(PREFS_NAME, 0);
	    surgery_id = settings.getString("surgery_id", "");
	    pms = settings.getString("pms", "");
	    
	    Log.i(TAG, pms);   
	    
        View addToWaitBtnClick = findViewById(R.id.noMedicareCardBtn);
        addToWaitBtnClick.setOnClickListener(this);
       
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(this.getResources().getColor(R.color.black));
        
        android_id = Secure.getString(getApplicationContext().getContentResolver(), Secure.ANDROID_ID);
        
        Log.i("android_id", android_id);
        
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey("sub-c-3001d7ac-0cf5-11e7-930d-02ee2ddab7fe");
        pnConfiguration.setPublishKey("pub-c-5a3f26dc-b33f-47a4-bac4-1f49d94f0ea6");
        pnConfiguration.setSecure(false);
        
        pubnub = new PubNub(pnConfiguration);
        
        pubnub.addListener(new SubscribeCallback() {
            @Override
         
            public void status(PubNub pubnub, PNStatus status) {
                // the status object returned is always related to subscribe but could contain
                // information about subscribe, heartbeat, or errors
                // use the operationType to switch on different options
            	
                switch (status.getOperation()) {
                    // let's combine unsubscribe and subscribe handling for ease of use
                    case PNSubscribeOperation:
                    	
                    case PNUnsubscribeOperation:
                        // note: subscribe statuses never have traditional
                        // errors, they just have categories to represent the
                        // different issues or successes that occur as part of subscribe
                        switch(status.getCategory()) {
                            case PNConnectedCategory:
                                // this is expected for a subscribe, this means there is no error or issue whatsoever
                            case PNReconnectedCategory:
                                // this usually occurs if subscribe temporarily fails but reconnects. This means
                                // there was an error but there is no longer any issue
                            case PNDisconnectedCategory:
                                // this is the expected category for an unsubscribe. This means there
                                // was no error in unsubscribing from everything
                            case PNUnexpectedDisconnectCategory:
                                // this is usually an issue with the internet connection, this is an error, handle appropriately
                            case PNAccessDeniedCategory:
                                // this means that PAM does allow this client to subscribe to this
                                // channel and channel group configuration. This is another explicit error
                            default:
                                // More errors can be directly specified by creating explicit cases for other
                                // error categories of `PNStatusCategory` such as `PNTimeoutCategory` or `PNMalformedFilterExpressionCategory` or `PNDecryptionErrorCategory`
                        }
                         
                    case PNHeartbeatOperation:
                        // heartbeat operations can in fact have errors, so it is important to check first for an error.
                        // For more information on how to configure heartbeat notifications through the status
                        // PNObjectEventListener callback, consult <link to the PNCONFIGURATION heartbeart config>
                        if (status.isError()) {
                            // There was an error with the heartbeat operation, handle here
                        } else {
                            // heartbeat operation was successful
                        }
                    default: {
                        // Encountered unknown status type
                    }
                }
            }
         
            @Override
            public void message(PubNub pubnub, PNMessageResult message) {
            	final JsonElement json = message.getMessage();
            	
            	MedicareSwipeActivity.this.runOnUiThread(new Runnable() {
            	    @Override
            	    public void run() {
            	    	showAlertDialog(json.toString());
            	    }
            	});
            }
         
            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult presence) {
                // handle incoming presence data
            }
        });
        
    }

  
    private void showAlertDialog(String message) {
    	
    	pd.dismiss();
    	Log.i("pubnub", message);
    	
    	message = message.replace("\"", "");
    	message = message.replace("*", "\n");
    	
    	final Dialog dialog = new Dialog(this);
    	dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    	dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);     
    	dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    	dialog.setContentView(R.layout.my_dialog);
    	
    	TextView messageTextView = (TextView)dialog.findViewById(R.id.messageTextView);
        messageTextView.setText(message);
        messageTextView.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        ImageView checkedInImage = (ImageView)dialog.findViewById(R.id.imageCheckedIn);
        
        if (message.indexOf("front desk") > -1) {
        	checkedInImage.setImageResource(R.drawable.no_checkin_btn);
    	} else {
    		checkedInImage.setImageResource(R.drawable.checked_in);
    	}
        Button dialogButton = (Button) dialog.findViewById(R.id.doneBtn);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		
        dialog.show();
        
    }
    
    
    protected void OnCardDataReceived(IMTCardData cardData)
    {
	    //clearDisplay();
    	String CurrentString = cardData.getTrack2Masked();
    	String[] separated = CurrentString.split("=");
    	String mcNumber = separated[1]+separated[2];
    	
    	PubNubData message = new PubNubData();
		
		message.my1stPatientCheckin = android_id;
		message.p = pms;
		message.f = "";
		message.l = "";
		message.d = "";
		message.g = "";
		message.m = mcNumber;
		
		Log.i("message.my1stPatientCheckin", message.my1stPatientCheckin);
		Log.i("message.m", message.m);
		
		String checkin_channel = "MH1PCI_" + surgery_id;
		
		pubnub.publish().channel(checkin_channel).message(message).async(new PNCallback<PNPublishResult>() {
            public void onResponse(PNPublishResult result, PNStatus status) {
            	
            }
        });
		
    	Log.i("Card swiped", mcNumber);  	
    }  
   
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    
    class PubNubData {
        String my1stPatientCheckin;
        String p;
        String f;
        String l;
        String d;
        String g;
        String m;
    } 
    
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		
			case R.id.noMedicareCardBtn:
				settings = getSharedPreferences(PREFS_NAME, 0);
				final SharedPreferences.Editor editor = settings.edit();
				editor.putString("hasMedicareCard", "no");
				editor.commit();
				Intent ms = new Intent(MedicareSwipeActivity.this, MainActivity.class);  
				startActivity(ms);
				break;
			}
		
		}

}