package com.FirstAvailable.PatientCheckIn;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.JsonElement;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.PNCallback;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.models.consumer.PNPublishResult;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

@SuppressLint({ "ClickableViewAccessibility", "CutPasteId" })
public class MainActivity extends Activity  implements OnClickListener, OnFocusChangeListener, OnTouchListener {

	public JSONArray doctors_array;
	public JSONArray surgery_status_array;
	public Spinner doctorsSpinner;
	public static String practitionerID;
	public static String doctorCode;
	public static String gender;
	public ProgressDialog pd;
	private AlertDialog alert;
	public static final String PREFS_NAME = "MyPrefsFile";
	public static SharedPreferences settings = null;
	public static String surgery_id;
	public static String pms;
	public static String swipe;
	public static String hasMedicareCard;
	public static String url;
	public static boolean complete;
	public static String logo;
	public static String logo_landscape;
	public PubNub pubnub;
	public String android_id;
	
	DateDialogFragment frag;
	Button button;
    Calendar now;
    
    Bitmap b;
    ImageView practiceLogo;
    
    Bitmap l;
    ImageView practiceLogoLandscape;
    
   
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(this.getResources().getColor(R.color.black));
        
        practiceLogo = (ImageView)findViewById(R.id.practiceLogo);
        
        logo = "";
        logo_landscape = "";
        gender = "Female";
        complete = false;
        
        android_id = Secure.getString(getApplicationContext().getContentResolver(), Secure.ANDROID_ID);
        
        Log.i("android_id", android_id);
        
        View addToWaitBtnClick = findViewById(R.id.addToWaitBtn);
        addToWaitBtnClick.setOnClickListener(this);
        
        View femaleBtnClick = findViewById(R.id.female_btn);
        femaleBtnClick.setOnClickListener(this);
        
        View maleBtnClick = findViewById(R.id.male_btn);
        maleBtnClick.setOnClickListener(this);
        
        View mcBtnClick = findViewById(R.id.mcBackBtn);
        mcBtnClick.setOnClickListener(this);
        
        Button female_btn = (Button)findViewById(R.id.female_btn);
    	Button male_btn = (Button)findViewById(R.id.male_btn);
    	female_btn.setTextColor(getResources().getColor(R.color.banner_title));
    	male_btn.setTextColor(getResources().getColor(R.color.labels));
    	
        now = Calendar.getInstance();
        
        button = (Button)findViewById(R.id.dob_btn);
        button.setText("");
        button.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View v) {
        		showDialog();	
        	}
        });
        
        PNConfiguration pnConfiguration = new PNConfiguration();
        pnConfiguration.setSubscribeKey("sub-c-3001d7ac-0cf5-11e7-930d-02ee2ddab7fe");
        pnConfiguration.setPublishKey("pub-c-5a3f26dc-b33f-47a4-bac4-1f49d94f0ea6");
        pnConfiguration.setSecure(false);
        
        pubnub = new PubNub(pnConfiguration);
        
        pubnub.addListener(new SubscribeCallback() {
            @Override
         
            public void status(PubNub pubnub, PNStatus status) {
                // the status object returned is always related to subscribe but could contain
                // information about subscribe, heartbeat, or errors
                // use the operationType to switch on different options
            	
                switch (status.getOperation()) {
                    // let's combine unsubscribe and subscribe handling for ease of use
                    case PNSubscribeOperation:
                    	
                    case PNUnsubscribeOperation:
                        // note: subscribe statuses never have traditional
                        // errors, they just have categories to represent the
                        // different issues or successes that occur as part of subscribe
                        switch(status.getCategory()) {
                            case PNConnectedCategory:
                                // this is expected for a subscribe, this means there is no error or issue whatsoever
                            case PNReconnectedCategory:
                                // this usually occurs if subscribe temporarily fails but reconnects. This means
                                // there was an error but there is no longer any issue
                            case PNDisconnectedCategory:
                                // this is the expected category for an unsubscribe. This means there
                                // was no error in unsubscribing from everything
                            case PNUnexpectedDisconnectCategory:
                                // this is usually an issue with the internet connection, this is an error, handle appropriately
                            case PNAccessDeniedCategory:
                                // this means that PAM does allow this client to subscribe to this
                                // channel and channel group configuration. This is another explicit error
                            default:
                                // More errors can be directly specified by creating explicit cases for other
                                // error categories of `PNStatusCategory` such as `PNTimeoutCategory` or `PNMalformedFilterExpressionCategory` or `PNDecryptionErrorCategory`
                        }
                         
                    case PNHeartbeatOperation:
                        // heartbeat operations can in fact have errors, so it is important to check first for an error.
                        // For more information on how to configure heartbeat notifications through the status
                        // PNObjectEventListener callback, consult <link to the PNCONFIGURATION heartbeart config>
                        if (status.isError()) {
                            // There was an error with the heartbeat operation, handle here
                        } else {
                            // heartbeat operation was successful
                        }
                    default: {
                        // Encountered unknown status type
                    }
                }
            }
         
            @Override
            public void message(PubNub pubnub, PNMessageResult message) {
            	final JsonElement json = message.getMessage();
            	
            	MainActivity.this.runOnUiThread(new Runnable() {
            	    @Override
            	    public void run() {
            	    	showAlertDialog(json.toString());
            	    }
            	});
            }
         
            @Override
            public void presence(PubNub pubnub, PNPresenceEventResult presence) {
                // handle incoming presence data
            }
        });
        
        checkStatus();
        
        //getDoctors();
    }

  
    private void showAlertDialog(String message) {
    	
    	pd.dismiss();
    	Log.i("pubnub", message);
    	
    	message = message.replace("\"", "");
    	message = message.replace("*", "\n");
    	
    	final Dialog dialog = new Dialog(this);
    	dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    	dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);     
    	dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    	dialog.setContentView(R.layout.my_dialog);
    	
    	TextView messageTextView = (TextView)dialog.findViewById(R.id.messageTextView);
        
        messageTextView.setText(message);
        messageTextView.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        ImageView checkedInImage = (ImageView)dialog.findViewById(R.id.imageCheckedIn);
       
        if (message.indexOf("front desk") > -1) {
        	checkedInImage.setImageResource(R.drawable.no_checkin_btn);
    	} else {
    		checkedInImage.setImageResource(R.drawable.checked_in);
    	}
        Button dialogButton = (Button) dialog.findViewById(R.id.doneBtn);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				settings = getSharedPreferences(PREFS_NAME, 0);
		    	final SharedPreferences.Editor editor = settings.edit();
		    	editor.putString("hasMedicareCard", "yes");
		    	editor.commit();
		    	swipe = settings.getString("swipe", "");
		    	Log.i("swipe", swipe);
			    if (swipe.equals("yes")) {
		        	Intent ms = new Intent(MainActivity.this, MedicareSwipeActivity.class);  
					startActivity(ms);
		        }
			}
		});
		
        dialog.show();
        now = Calendar.getInstance();
        
        button = (Button)findViewById(R.id.dob_btn);
        button.setText("Enter your date of birth");

		Button completeButton = (Button)findViewById(R.id.addToWaitBtn);
		String rotation = getRotation(MainActivity.this);
		
        	if (rotation.equals("landscape")) {
        		completeButton.setBackgroundResource(R.drawable.kiosk_button_landscape);
        	} else {
        		completeButton.setBackgroundResource(R.drawable.kiosk_button);
        	}
        	
        complete = false;
		gender = "Female";		
		setGender("female");
		
    }
    
    public void showResponse(String message) {
    	pd.dismiss();
    	Log.i("pubnub", message);
    	message = message.replace("\"", "");
    	message = message.replace("*", "\n");
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
  		builder.setTitle("Patient Check In Result")
  			   .setMessage(message)
  		       .setCancelable(false)
  		       .setNegativeButton("OK", new DialogInterface.OnClickListener() {
  		           public void onClick(DialogInterface dialog, int id) {
  		        	 
  		              dialog.cancel();
  		              
  		           }
  		       });
  		alert = builder.create();
  		alert.show();
  		TextView textView = (TextView) alert.findViewById(android.R.id.message);
  	    textView.setTextSize(30);
  		
  		now = Calendar.getInstance();
        
        button = (Button)findViewById(R.id.dob_btn);
        button.setText("Enter your date of birth");

		Button completeButton = (Button)findViewById(R.id.addToWaitBtn);
		String rotation = getRotation(MainActivity.this);
		
        	if (rotation.equals("landscape")) {
        		completeButton.setBackgroundResource(R.drawable.kiosk_button_landscape);
        	} else {
        		completeButton.setBackgroundResource(R.drawable.kiosk_button);
        	}
        
        
        complete = false;
		gender = "Female";		
		setGender("female");
	    
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    public void showDialog() {
    	hideKeyboard();
    	FragmentTransaction ft = getFragmentManager().beginTransaction(); //get the fragment
    	frag = DateDialogFragment.newInstance(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new DateDialogFragmentListener(){
    		public void updateChangedDate(int year, int month, int day){
    			String monthString = "";
    			switch(month) {
    				case 0:
    					monthString = "January";
    					break;
    				case 1:
        				monthString = "February";
        				break;
    				case 2:
        				monthString = "March";
        				break;
    				case 3:
        				monthString = "April";
        				break;
    				case 4:
        				monthString = "May";
        				break;
    				case 5:
        				monthString = "June";
        				break;
    				case 6:
        				monthString = "July";
        				break;
    				case 7:
        				monthString = "August";
        				break;
    				case 8:
        				monthString = "September";
        				break;
    				case 9:
        				monthString = "October";
        				break;
    				case 10:
        				monthString = "November";
        				break;
    				case 11:
        				monthString = "December";
        				break;
    			}
    			hideKeyboard();
    			button.setText(String.valueOf(day)+" "+monthString+" "+String.valueOf(year));
    			now.set(year, month, day);
    			checkComplete();
    		}
    	}, now);
    	
    	frag.show(ft, "DateDialogFragment");
    	
    }
    
    public void hideKeyboard() {
    	InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
    	View view = this.getCurrentFocus();
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        View mainView = findViewById(R.id.formLayout);
        mainView.requestFocus();
    }
    
    public void setGender(String gender) {
    	button = (Button)findViewById(R.id.dob_btn);
		String dob = (String) button.getText();
    	Log.i("setGender DOB", dob);
    	Button female_btn = (Button)findViewById(R.id.female_btn);
    	Button male_btn = (Button)findViewById(R.id.male_btn);
    	if (gender.equals("female")) {
    		female_btn.setBackgroundResource(R.drawable.button_left_bg_pressed);
    		female_btn.setTextColor(getResources().getColor(R.color.banner_title));
    		male_btn.setBackgroundResource(R.drawable.button_right_bg);
    		male_btn.setTextColor(getResources().getColor(R.color.labels));
    	} else {
    		female_btn.setBackgroundResource(R.drawable.button_left_bg);
    		female_btn.setTextColor(getResources().getColor(R.color.labels));
    		male_btn.setBackgroundResource(R.drawable.button_right_bg_pressed);
    		male_btn.setTextColor(getResources().getColor(R.color.banner_title));
    	}
    	
    }
    
    public interface DateDialogFragmentListener{
    	//this interface is a listener between the Date Dialog fragment and the activity to update the buttons date
    	public void updateChangedDate(int year, int month, int day);
    }
    
    public void checkStatus() {
    	
    	settings = getSharedPreferences(PREFS_NAME, 0);
	    surgery_id = settings.getString("surgery_id", "");
	    
	    if (surgery_id.equals("")) {
	    	AlertDialog.Builder builder = new AlertDialog.Builder(this);

			builder.setTitle("Set Surgery ID");
			builder.setMessage("The surgery's ID number has not been set.\nPlease enter the surgery's ID number.\nThe surgery's ID number can be obtained from DocAppointment.");

			// Set an EditText view to get user input
			final EditText input = new EditText(this);
			input.setLines(1);
			input.setInputType(InputType.TYPE_CLASS_TEXT);
			
			input.setImeActionLabel("Custom text", KeyEvent.KEYCODE_ENTER);

			builder.setView(input);

			builder.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int whichButton) {
							Editable value = input.getText();
							surgery_id = value.toString();
							SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
							final SharedPreferences.Editor editor = settings.edit();
							editor.putString("surgery_id", surgery_id);
							editor.commit();
							getSurgeryStatus();
						}
					});

			builder.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
				
						public void onClick(DialogInterface dialog,
								int whichButton) {
							dialog.cancel();
							return;
						}
					});

			alert = builder.create();
			alert.show();
			
	    } else {
	    	
	    	getSurgeryStatus();
	    	
	    }
    	
    }
    
    public void getSurgeryStatus() {
    	
    	pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Checking surgery status...");
        pd.show();
        
    	Log.i("Getting Surgery Status", "getting surgery status");
    	Log.i("Getting Surgery Status URL", "https://secure.docappointment.com.au/apps/wating_room_init.php?surgery_id="+surgery_id);
    	AsyncHttpClient client = new AsyncHttpClient();
        client.get("https://secure.docappointment.com.au/apps/wating_room_init.php?surgery_id="+surgery_id, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(String response) {
            	loadSurgeryStatusJSONResult(response);
            }
        });
    	
    }
    
    public void loadSurgeryStatusJSONResult(String response) {
 
	    	pd.dismiss();
	    	
	    	JSONObject json = null;
	   	 	
			try {
				json = new JSONObject(response);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}    
	        
	        try{
	        
	        	JSONArray result = json.getJSONArray("result");
	        	JSONObject e = result.getJSONObject(0);
	        	
	        	Log.i("Check surgery status result", result.toString());
	        		pms = e.getString("pms");
	        		swipe = e.getString("swipe");
	        		SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
					final SharedPreferences.Editor editor = settings.edit();
					editor.putString("pms", pms);
					editor.commit();
					editor.putString("swipe", swipe);
					editor.commit();
	        		logo = e.getString("logo");
	        		logo_landscape = e.getString("logo_landscape");
	        		information info = new information();
	                info.execute("");
	        		
	                Log.i("pms", pms);
	                Log.i("swipe", swipe);
	        		Log.i("logo", logo);
	        		Log.i("logo_landscape", logo_landscape);
	        		
	        		String channel = android_id;
	                pubnub.subscribe().channels(Arrays.asList(channel)).execute();
	                
	                hasMedicareCard = settings.getString("hasMedicareCard", "");
	                
	                if (swipe.equals("yes") && !hasMedicareCard.equals("no") ) {
	                	Intent ms = new Intent(MainActivity.this, MedicareSwipeActivity.class);  
	    				startActivity(ms);
	                } else {
	                	//Button no_mc_btn = (Button)findViewById(R.id.mcBackBtn);
	                	//no_mc_btn.setVisibility(View.GONE);
	                	editor.putString("hasMedicareCard", "no");
						editor.commit();
	                }
	                
	        }catch(JSONException e)        {
	        	 Log.e("log_tag", "Error parsing data "+e.toString());
	        }
	        
	    }
    
    class PubNubData {
        String my1stPatientCheckin;
        String p;
        String f;
        String l;
        String d;
        String g;
        String m;
    } 
    
    public void addToWaitingRoom() {
    	
    	if (isValidInput()) {
    	pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Letting the front desk know you have arrived...");
        pd.show();
        
		button = (Button)findViewById(R.id.dob_btn);
		String dob = (String) button.getText();
		String[] separated = dob.split(" ");
		String month = separated[1];
		
		if (month.equals("January")) {
			month = "01";
		} else if (month.equals("February")) {
			month = "02";
		} else if (month.equals("March")) {
			month = "03";
		} else if (month.equals("April")) {
			month = "04";
		} else if (month.equals("May")) {
			month = "05";
		} else if (month.equals("June")) {
			month = "06";
		} else if (month.equals("July")) {
			month = "07";
		} else if (month.equals("August")) {
			month = "08";
		} else if (month.equals("September")) {
			month = "09";
		} else if (month.equals("October")) {
			month = "10";
		} else if (month.equals("November")) {
			month = "11";
		} else if (month.equals("December")) {
			month = "12";
		}
		
		if (separated[0].length() == 1) {
			separated[0] = "0" + separated[0];
		}
		dob = separated[2] + "-" + month + "-" + separated[0];
		Log.i("dob", dob);
		
		PubNubData message = new PubNubData();
		
		message.my1stPatientCheckin = android_id;
		message.p = pms;
		message.f = "";
		message.l = "";
		message.d = dob;
		message.g = gender;
		message.m = "";
		
		settings = getSharedPreferences(PREFS_NAME, 0);
	    surgery_id = settings.getString("surgery_id", "");
	    
	    String checkin_channel = "MH1PCI_" + surgery_id;
	    Log.i("checkin_channel", checkin_channel + " = " + message.toString());
		pubnub.publish().channel(checkin_channel).message(message).async(new PNCallback<PNPublishResult>() {
            public void onResponse(PNPublishResult result, PNStatus status) {
            	
            }
        });
    	
    	}
    }
    
    
    private void checkComplete() {
    	Log.i("checkComplete", "checkComplete");
    	button = (Button)findViewById(R.id.dob_btn);
		String dob = (String) button.getText();
		Log.i("dob", dob);
		Button completeButton = (Button)findViewById(R.id.addToWaitBtn);
		String rotation = getRotation(MainActivity.this);
		if (!dob.equals("Enter your date of birth")) {
			Log.i("checkComplete", "!= Enter your date of birth");
        	if (rotation.equals("landscape")) {
        		completeButton.setBackgroundResource(R.drawable.kiosk_button_complete_landscape);
        	} else {
        		completeButton.setBackgroundResource(R.drawable.kiosk_button_complete);
        	}
			complete = true;
		} else {
			Log.i("checkComplete", "== Enter your date of birth");
			if (rotation.equals("landscape")) {
        		completeButton.setBackgroundResource(R.drawable.kiosk_button_landscape);
        	} else {
        		completeButton.setBackgroundResource(R.drawable.kiosk_button);
        	}
			complete = false;
		}
		hideKeyboard();
    }
    
    private boolean isValidInput() {
		
    	button = (Button)findViewById(R.id.dob_btn);
		String dob = (String) button.getText();
		if (dob.equals("Enter your date of birth")) {
			showerror("No DOB",
					"Please enter your date of birth.");
			return false;
		} 
		return true;
	}

	void showerror(String title, String msg) {
		MyConstants.showAlert(this, title, msg, "OK");
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId()){
		
			case R.id.female_btn:
				gender = "Female";		
				setGender("female");
				break;
				
			case R.id.male_btn:
				gender = "Male";
				setGender("male");
				break;
				
			case R.id.mcBackBtn:
				settings = getSharedPreferences(PREFS_NAME, 0);
		    	final SharedPreferences.Editor editor = settings.edit();
		    	editor.putString("hasMedicareCard", "yes");
		    	editor.commit();
		    	swipe = settings.getString("swipe", "");
		    	Log.i("swipe", swipe);
			    if (swipe.equals("yes")) {
		        	Intent ms = new Intent(MainActivity.this, MedicareSwipeActivity.class);  
					startActivity(ms);
		        }
				break;
				
			case R.id.addToWaitBtn:
				
				if (complete) {
					addToWaitingRoom();
				} else {
					isValidInput();
				}
				
				break;
		
			}
		
		}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		switch(v.getId()){
        
		}
	}
	
	public class information extends AsyncTask<String, String, String>
    {
        @Override
        protected String doInBackground(String... arg0) {

            try
            {
                URL url = new URL(logo);
                InputStream is = new BufferedInputStream(url.openStream());
                b = BitmapFactory.decodeStream(is);
                
                URL urlLandscape = new URL(logo_landscape);
                InputStream isl = new BufferedInputStream(urlLandscape.openStream());
                l = BitmapFactory.decodeStream(isl);

            } catch(Exception e){}
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
        	
        	String rotation = getRotation(MainActivity.this);
        	if (rotation.equals("landscape")) {
        		practiceLogo.setImageBitmap(b);
        	} else {
        		practiceLogo.setImageBitmap(l);
        	}
        	
        }
    }
	
	public String getRotation(Context context){
	    final int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
	           switch (rotation) {
	            case Surface.ROTATION_0:
	                return "portrait";
	            case Surface.ROTATION_90:
	                return "landscape";
	            case Surface.ROTATION_180:
	                return "reverse portrait";
	            default:
	                return "reverse landscape";
	            }
	        }
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);  
	    information info = new information();
        info.execute("");
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		hideKeyboard();// TODO Auto-generated method stub
		return false;
	}
	
}

